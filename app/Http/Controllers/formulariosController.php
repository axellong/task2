<?php

namespace App\Http\Controllers;

use App\Usuario;
use Illuminate\Http\Request;

class formulariosController extends Controller
{
    public function getV()
    {
        $usuarios = Usuario::get();
        return view("formularios.get", ["usuarios" => $usuarios]);
    }

    public function postV($variable = true)
    {
        return view("formularios.post", ["variable" => $variable]);
    }

    public function updateV($variable = true)
    {
        return view("formularios.put", ["variable" => $variable]);
    }

    public function deleteV($variable = "no")
    {
        return view("formularios.delete", ["variable" => $variable]);
    }

    public function postF(Request $request)
    {
        $nuevoUsuario = new Usuario();
        $nuevoUsuario->name = $request->name;
        $nuevoUsuario->lastname = $request->lastname;
        $nuevoUsuario->age = $request->age;
        $nuevoUsuario->save();
        return redirect()->route("postV", ["variable" => $nuevoUsuario]);
    }

    public function updateF(Request $request)
    {
        $usuarioModificar = Usuario::find($request->id);
        $usuarioModificar->name = $request->name;
        $usuarioModificar->lastname = $request->lastname;
        $usuarioModificar->age = $request->age;
        $usuarioModificar->save();
        return redirect()->route("updateV", ["variable" => $usuarioModificar]);
    }

    public function deleteF(Request $request)
    {
        $usuarioEliminado = Usuario::find($request->id);
        $usuarioEliminado->delete();
        return redirect()->route("deleteV", ["variable" => "si"]);
    }
}
