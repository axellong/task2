@extends('plantilla.base')
@section('css')
    <link rel="stylesheet" href="{{ asset('css/formulario.css') }}">
    <link rel="stylesheet" href="{{ asset('css/get.css') }}">
@endsection
@section('titulo')
    inicio
@endsection

@section('navbar')
    @extends('plantilla.navbar')
@endsection
@section('contenido')
    <p class="titulo">USUARIOS</p>
    <ol style="padding: 0">
        @foreach ($usuarios as $item)
        <li class="item">{{$item->name}} {{$item->lastname}} id: {{$item->id}}</li>
        @endforeach
    </ol>
@endsection
