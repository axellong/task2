@extends('plantilla.formulario')
@section('titulo')
    Actualizar
@endsection


@section('navbar')
    @extends('plantilla.navbar')
@endsection

@section('tituloF')
    <h1>Actualizar Usuario</h1>
@endsection

@section('metodo')
    @method("put")
@endsection

@section('inputSeleccion')
    <input type="number" required class="input" name="id" placeholder="id a modoficar">
@endsection

@section('formulario')
    method="POST" action="{{ route('updateF') }}"
@endsection


@section('submitValue')
    value="Actualizar"
@endsection

@section('notificacion')
    @if ($variable != 1)
        <p style="margin-left: 40.5%; font-size:20px">se actualizo correctamente el id {{ $variable }}</p>
    @endif
@endsection
