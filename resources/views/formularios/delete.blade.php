@extends('plantilla.base')
@section('css')
    <link rel="stylesheet" href="{{ asset('css/formulario.css') }}">
    <link rel="stylesheet" href="{{ asset('css/get.css') }}">
@endsection
@section('titulo')
    Eliminar
@endsection

@section('navbar')
    @extends('plantilla.navbar')
@endsection
@section('contenido')
    <form class="formulario" action="{{ route('deleteF') }}" method="post">
        <h1>Eliminar Usuario</h1>
        @csrf
        @method("delete")
        <input required name="id" class="input" type="number" placeholder="id a eliminar">
        <input class="input submir" type="submit">
    </form>
    @if ($variable == 'si')
        <p style="margin-left: 40.5%; font-size:20px">se elimino correctamente</p>
    @endif
@endsection
