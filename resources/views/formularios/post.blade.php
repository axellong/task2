@extends('plantilla.formulario')
@section('css')
    {{-- <link rel="stylesheet" href="{{asset("css/post.css")}}"> --}}

@endsection
@section('titulo')
    Crear
@endsection
@section('navbar')
    @extends('plantilla.navbar')
@endsection

@section('tituloF')
    <h1>Creacion nuevo Usuario</h1>
@endsection

@section('formulario')
    method="POST" action="{{ route('postF') }}"
@endsection
@section('submitValue')
    value="Crear"
@endsection

@section('notificacion')
    @if ($variable != 1)
        <p style="margin-left: 40.5%; font-size:20px">se agrego correctamente con el id {{ $variable }}</p>
    @endif
@endsection
