<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/formulario.css') }}">
    <title>@yield('titulo')</title>
    @yield('css')
</head>

<body>
    @yield('navbar')

    <form class="formulario" @yield('formulario')>
        @yield('tituloF')
        @csrf
        @yield('metodo')
        @yield('inputSeleccion')
        <input required name="name" class="input" type="text" placeholder="nombre">
        <input required name="lastname" class="input" type="text" placeholder="apellido">
        <input required name="age" class="input" type="number" placeholder="edad">
        <input class="input submit" type="submit" @yield('submitValue')>
    </form>
    @yield('notificacion')



</body>

</html>
