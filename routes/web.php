<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\formulariosController;

Route::get('/', "formulariosController@getV")->name("getV");
Route::get('/post/{variable?}', "formulariosController@postV")->name("postV");
Route::post("/delete/formulario","formulariosController@postF")->name("postF");
Route::get('/update/{variable?}', "formulariosController@updateV")->name("updateV");
Route::put('/update/formulario', "formulariosController@updateF")->name("updateF");
Route::get('/delete/{variable?}', "formulariosController@deleteV")->name("deleteV");
Route::delete('/delete/{variable?}', "formulariosController@deleteF")->name("deleteF");
